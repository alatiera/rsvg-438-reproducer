use cairo;
use librsvg;

fn main() {
    let input = "org.gnome.SocialExperiment.svg";
    let width = 96.0;
    let height = 96.0;
    let output = "output.svg";

    let handle = librsvg::Loader::new().read_path(input).unwrap();
    let renderer = librsvg::CairoRenderer::new(&handle);

    let surface = cairo::svg::File::new(width, height, output);
    let cr = cairo::Context::new(&surface);
    renderer
        .render_element_to_viewport(
            &cr,
            None,
            &cairo::Rectangle {
                x: 0.0,
                y: 0.0,
                width,
                height,
            },
        )
        .unwrap();
}

